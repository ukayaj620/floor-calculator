import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { ROUTES } from "../constants/routes";
import AboutScreen from "../screens/about";
import MainScreen from "../screens/main";

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen
        name={ROUTES.MAIN}
        component={MainScreen}
        options={{ title: "Main" }}
      />
      <Drawer.Screen
        name={ROUTES.ABOUT}
        component={AboutScreen}
        options={{ title: "About" }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
