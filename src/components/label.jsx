import React from "react";
import { Text } from "react-native";

const TextLabel = ({ text, color = "#111827", textAlign = "justify" }) => {
  return (
    <Text
      style={{
        fontSize: 16,
        fontWeight: "500",
        color: color,
        textAlign: textAlign,
      }}
    >
      {text}
    </Text>
  );
};

export default TextLabel;
