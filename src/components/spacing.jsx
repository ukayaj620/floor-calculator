import React from "react";
import { View } from "react-native";

const Spacing = ({ x = 0, y = 0 }) => {
  return <View style={{ width: x, height: y }} />;
};

export default Spacing;
