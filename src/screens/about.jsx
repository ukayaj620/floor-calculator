import React from "react";
import { StyleSheet, View } from "react-native";

import TextLabel from "../components/label";
import Spacing from "../components/spacing";

const AboutScreen = () => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <View style={styles.card}>
        <TextLabel text={`Full Name: Johannes Edwin`} />
        <Spacing y={8} />
        <TextLabel text={`Student Id: 123456789`} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#FAFAFA",
    padding: 16,
    borderRadius: 8,
  },
});

export default AboutScreen;
