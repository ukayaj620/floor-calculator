import React, { useState } from "react";
import { StyleSheet, TextInput, TouchableOpacity, View } from "react-native";
import { Switch } from "react-native-gesture-handler";

import TextLabel from "../components/label";
import Spacing from "../components/spacing";

const MainScreen = () => {
  const [size, setSize] = useState(0);
  const [flooringPrice, setFlooringPrice] = useState(0);
  const [installationPrice, setInstallationPrice] = useState(0);

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((prevState) => !prevState);

  const [isCalculated, setIsCalculated] = useState(false);
  const showCalculation = () => setIsCalculated(true);
  const resetCalculation = () => {
    setIsCalculated(false);
    setSize(0);
    setFlooringPrice(0);
    setInstallationPrice(0);
  };

  const getSizeUnit = () => (isEnabled ? "ft²" : "m²");

  const flooringCost = size * flooringPrice;
  const installationCost = size * installationPrice;
  const totalCost = flooringCost + installationCost;
  const tax = totalCost * 0.13;

  return (
    <View style={{ flex: 1, padding: 16 }}>
      <TextLabel text={"Size"} />
      <Spacing y={8} />
      <View style={styles.row}>
        <View style={styles.row}>
          <TextInput style={styles.input} onChangeText={setSize} value={size} />
          <TextLabel text={getSizeUnit()} />
        </View>
        <Switch
          trackColor={{ false: "#047857", true: "#1D4ED8" }}
          thumbColor={isEnabled ? "#60A5FA" : "#34D399"}
          ios_backgroundColor="#374151"
          onValueChange={toggleSwitch}
          value={isEnabled}
        />
      </View>
      <Spacing y={16} />
      <TextLabel text={"Flooring Price"} />
      <Spacing y={8} />
      <View style={styles.row}>
        <TextInput
          style={styles.input}
          onChangeText={setFlooringPrice}
          value={flooringPrice}
        />
      </View>
      <Spacing y={16} />
      <TextLabel text={"Installation Price"} />
      <Spacing y={8} />
      <View style={styles.row}>
        <TextInput
          style={styles.input}
          onChangeText={setInstallationPrice}
          value={installationPrice}
        />
      </View>
      <Spacing y={24} />
      <TouchableOpacity style={styles.button} onPress={showCalculation}>
        <TextLabel
          text={"Installation Price"}
          color="#F9FAFB"
          textAlign="center"
        />
      </TouchableOpacity>
      <Spacing y={24} />
      {isCalculated && (
        <View style={styles.result}>
          <TextLabel text={`Installation Cost: ${installationCost}`} />
          <Spacing y={4} />
          <TextLabel text={`Flooring Cost: ${flooringCost}`} />
          <Spacing y={4} />
          <TextLabel text={`Total Cost: ${totalCost}`} />
          <Spacing y={4} />
          <TextLabel text={`Tax: ${tax}`} />
          <Spacing y={24} />
          <TouchableOpacity style={styles.button} onPress={resetCalculation}>
            <TextLabel
              text={"Reset Calculation"}
              color="#F9FAFB"
              textAlign="center"
            />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  input: {
    height: 40,
    marginRight: 8,
    borderWidth: 1,
    padding: 12,
    borderRadius: 8,
    width: 240,
    fontSize: 14,
  },
  label: {
    fontSize: 16,
    fontWeight: "500",
  },
  button: {
    fontSize: 16,
    backgroundColor: "#1F2937",
    paddingVertical: 8,
    paddingHorizontal: 16,
    textAlign: "center",
    borderRadius: 8,
    height: 40,
  },
  result: {
    backgroundColor: "#FAFAFA",
    padding: 16,
    borderRadius: 8,
  },
});

export default MainScreen;
